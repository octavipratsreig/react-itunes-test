import './Footer.scss';

export default function Footer() {
  return (
    <div className="footer container-row container-row-space-between-center">
        <span>SlashMobility React-Junior</span>
        <span>Octavi Prats Reig</span>
    </div>
  );
}
