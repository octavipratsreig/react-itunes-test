import { MdFavorite, MdFavoriteBorder } from "react-icons/md";
import { useDispatch } from "react-redux";
import { fav_unfav } from "../../../redux/actions";
import "./Song.scss";

export default function Song(props) {
  const dispatch = useDispatch();

  function toggleFav() {
    dispatch(fav_unfav(props.data))
  }

  return (
    <div className="song">
      <div className="container-column">
        <div className="container-row">
          <img src={props.data.artworkUrl100} alt="" />
          <div className="badge-list container-column">
            <span>{props.data.primaryGenreName}</span>
          </div>
        </div>
        <div className="container-column container-column-start-center">
          <span className="name">{props.data.trackName}</span>
          <span className="collection">{props.data.collectionName}</span>
          <span className="artist">{props.data.artistName}</span>
        </div>
      </div>
      <button onClick={toggleFav} className="btn-fav container-row container-row-center-center" >
        {props.data.fav ? <MdFavorite className="ic-fav" /> : <MdFavoriteBorder className="ic-fav"/>}
      </button>
    </div>
  );
}
