import Song from "./Song/Song";
import { useSelector } from "react-redux";
import './List.scss';

export default function List() {
  const songs = useSelector((state) => state.searchBar.songs);

  return (
    <div className="list container">
      <div className="container-row container-row-center-center">
        {songs.map((song) => (
          <Song data={song} key={song.trackId} />
        ))}
        {['e','x','t','r','a'].map((x) => (<div className="holder" key={x}></div>))}
      </div>
    </div>
  );
}
