import "./Header.scss";
import { useSelector } from "react-redux";
import IonSearchbar from "./ion-searchbar";
import { MdFavorite, MdMenu } from "react-icons/md";

export default function Header() {
  const counter = useSelector((state) => state.searchBar.favCount);

  return (
    <div className="header">
      <div className="container container-row container-row-space-between-center">
        <div className="container-row container-row-space-between-center">
          <MdMenu className="header-icons"/>
          <IonSearchbar />
        </div>
        <div className="fav-count container-row container-row-space-between-center">
          <MdFavorite className="header-icons"/>
          <span>{counter}</span>
        </div>
      </div>
    </div>
  );
}
