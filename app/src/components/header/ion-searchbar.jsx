import "./Header.scss";
import { useDispatch, useSelector } from "react-redux";
import { write, getSongs } from "../../redux/actions";
import { useEffect, useState } from "react";

export default function IonSearchBar() {
  const search = useSelector((state) => state.searchBar.artistName);

  const [debounce, setDebounce] = useState(true);
  const [input, setInput] = useState(search);

  const dispatch = useDispatch();

  function changeHandler(event) {
    event.persist();
    setInput(event.target.value);
    setTimeout(() => {
      setDebounce(!debounce);
    }, 1000);
  }

  useEffect(() => {
    dispatch(getSongs(input));
  }, [debounce]);

  return (
    <input
      className="ion-searchbar"
      type="text"
      name="artist"
      placeholder="artist"
      value={input}
      onChange={changeHandler}
    />
  );
}
