import axios from "axios";

//https://itunes.apple.com/search?term=nombre+artista&entity=song

class ITunesService {
  constructor() {
    let service = axios.create({
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Content-Type, Authorization",
      },
      baseURL: "https://itunes.apple.com",
    });
    service.interceptors.response.use(this.handleSuccess, this.handleError);
    this.service = service;
  }

  handleSuccess(response) {
    return response.data;
  }

  handleError = (error) => {
    return Promise.reject(error);
  };

  redirectTo = (document, path) => {
    document.location = path;
  };

  getSongsByArtistName(artistName, callback) {
    const path = `/search?term=${artistName}&entity=song`;
    return this.service.get(path).then((response) => callback(response));
  }
}

export default new ITunesService();
