import searchBar from './searchBar';
import {combineReducers} from 'redux';

const allReducers = combineReducers({
    searchBar,
})

export default allReducers;