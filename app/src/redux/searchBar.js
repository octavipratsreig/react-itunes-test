const initialState = {
  songs: [],
  loading: false,
  error: null,
  artistName: "drake",
  favCount: 0,
};

const searchBar = (state = initialState, action) => {
  switch (action.type) {
    case "WRITE":
      return {
        ...state,
        artistName: action.payload,
      };
    case "GET_SONGS_REQUESTED":
      return {
        ...state,
        artistName: action.payload,
        loading: true,
      };
    case "GET_SONGS_SUCCESS":
      return {
        ...state,
        loading: false,
        songs: action.songs,
        favCount: 0,
      };
    case "GET_SONGS_FAILED":
      return {
        ...state,
        loading: false,
        error: action.message,
      };
    case "FAV_UNFAV":
      let count = state.favCount;
      const newSongList = state.songs.map((s) => {
        if (s.trackId === action.payload.trackId) {
          s["fav"] = s["fav"] ? false : true;
          if (s["fav"]) count++;
          else count--;
        }
        return s;
      });
      return {
        ...state,
        songs: newSongList,
        favCount: count,
      };

    default:
      return state;
  }
};
export default searchBar;
