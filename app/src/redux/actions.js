export const fav_unfav = (song) => {
    return {
        type: 'FAV_UNFAV',
        payload: song
    };
};

export const getSongs = artist => {
    return {
        type: 'GET_SONGS_REQUESTED',
        payload: artist
    };
};