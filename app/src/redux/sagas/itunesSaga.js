import { call, put, takeEvery } from "redux-saga/effects";
import axios from "axios";

function getApi(artistName) {
  const path = `https://itunes.apple.com/search?term=${artistName}&entity=song`;
  return axios.get(path);
}

function* fetchSongs(action) {
  try {
    const songs = yield call(getApi,action.payload);
    yield put({ type: "GET_SONGS_SUCCESS", songs: songs.data.results });
  } catch (e) {
    // yield put({ type: "GET_SONGS_FAILED", message: e.message });
    console.info(e);
  }
}

function* itunesSaga() {
  yield takeEvery("GET_SONGS_REQUESTED", fetchSongs);
}

export default itunesSaga;
