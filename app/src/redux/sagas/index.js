import { all } from 'redux-saga/effects'
import itunesSaga from './itunesSaga';

export default function* rootSaga() {
    yield all([
        itunesSaga(),
    ])
}