import './App.scss';

import Header from './components/header/Header'
import Footer from './components/footer/Footer';
import List from './components/list/List';

import styled from 'styled-components';

const Layout = styled.div`
min-height: 100.1vh;
display: flex;
flex-direction: column;
`
const Main = styled.div`
flex: 1;
`

function App() {
  return (
    <Layout className="App">
      <Header/>
      <Main>
        <List/>
      </Main>
      <Footer/>
    </Layout>
  );
}

export default App;
